total 12632
lrwxrwxrwx 1 root root       4 sie 27  2018 sh.distrib -> dash
lrwxrwxrwx 1 root root       4 sie 27  2018 sh -> dash
lrwxrwxrwx 1 root root       4 sie 27  2018 rnano -> nano
lrwxrwxrwx 1 root root       4 cze  7  2019 rbash -> bash
lrwxrwxrwx 1 root root       4 cze 28 13:05 ping6 -> ping
lrwxrwxrwx 1 root root       4 cze 28 13:05 ping4 -> ping
lrwxrwxrwx 1 root root       4 lis 12  2018 lsmod -> kmod
lrwxrwxrwx 1 root root       6 sie 27  2018 open -> openvt
lrwxrwxrwx 1 root root       6 lip  4 14:35 bzless -> bzmore
lrwxrwxrwx 1 root root       6 lip  4 14:35 bzfgrep -> bzgrep
lrwxrwxrwx 1 root root       6 lip  4 14:35 bzegrep -> bzgrep
lrwxrwxrwx 1 root root       6 lip  4 14:35 bzcmp -> bzdiff
lrwxrwxrwx 1 root root       7 mar  6  2019 static-sh -> busybox
lrwxrwxrwx 1 root root       8 sie 27  2018 ypdomainname -> hostname
lrwxrwxrwx 1 root root       8 sie 27  2018 nisdomainname -> hostname
lrwxrwxrwx 1 root root       8 sie 27  2018 lessfile -> lesspipe
lrwxrwxrwx 1 root root       8 sie 27  2018 domainname -> hostname
lrwxrwxrwx 1 root root       8 sie 27  2018 dnsdomainname -> hostname
lrwxrwxrwx 1 root root      14 sie 27  2018 pidof -> /sbin/killall5
lrwxrwxrwx 1 root root      20 lis 15 16:01 systemd -> /lib/systemd/systemd
lrwxrwxrwx 1 root root      20 sie 27  2018 nc -> /etc/alternatives/nc
lrwxrwxrwx 1 root root      20 sie 27  2018 mt -> /etc/alternatives/mt
lrwxrwxrwx 1 root root      24 sie 27  2018 netcat -> /etc/alternatives/netcat
-rwxr-xr-x 1 root root      28 wrz 18 17:00 fgrep
-rwxr-xr-x 1 root root      28 wrz 18 17:00 egrep
-rwxr-xr-x 1 root root      89 kwi 26  2016 red
-rwxr-xr-x 1 root root     140 kwi 28  2017 zfgrep
-rwxr-xr-x 1 root root     140 kwi 28  2017 zegrep
-rwxr-xr-x 1 root root     946 gru 30  2017 which
-rwxr-xr-x 1 root root    1297 lip  4 14:35 bzmore
-rwxr-xr-x 1 root root    1777 kwi 28  2017 zcmp
-rwxr-xr-x 1 root root    1910 kwi 28  2017 zmore
-rwxr-xr-x 1 root root    1937 kwi 28  2017 zcat
-rwxr-xr-x 1 root root    2037 kwi 28  2017 zless
-rwxr-xr-x 1 root root    2131 kwi 28  2017 zforce
-rwxr-xr-x 1 root root    2140 lip  4 14:35 bzdiff
-rwxr-xr-x 1 root root    2301 kwi 28  2017 uncompress
-rwxr-xr-x 1 root root    2301 kwi 28  2017 gunzip
-rwxr-xr-x 1 root root    2762 sty 22  2018 unicode_start
-rwxr-xr-x 1 root root    3642 lip  4 14:35 bzgrep
-rwxr-xr-x 1 root root    4877 lip  4 14:35 bzexe
-rwxr-xr-x 1 root root    5047 kwi 28  2017 znew
-rwxr-xr-x 1 root root    5764 kwi 28  2017 zdiff
-rwxr-xr-x 1 root root    5927 kwi 28  2017 gzexe
-rwxr-xr-x 1 root root    5938 kwi 28  2017 zgrep
-rwxr-xr-x 1 root root    8564 gru  1  2017 lesspipe
-rwxr-xr-x 1 root root   10104 gru 30  2017 tempfile
-rwxr-xr-x 1 root root   10256 gru  1  2017 lessecho
-rwxr-xr-x 1 root root   10312 mar 21  2019 ntfs-3g.probe
-rwxr-xr-x 1 root root   10312 sty 22  2018 kbd_mode
-rwxr-xr-x 1 root root   10312 sty 22  2018 fgconsole
-rwxr-xr-x 1 root root   10312 sty 22  2018 chvt
-rwxr-xr-x 1 root root   10320 lis 15 16:01 systemd-ask-password
-rwxr-xr-x 1 root root   14328 sie 11  2016 ulockmgr_server
-rwxr-xr-x 1 root root   14328 kwi 21  2017 chacl
-rwxr-xr-x 1 root root   14328 lip  4 14:35 bzip2recover
-rwxr-xr-x 1 root root   14400 lis 15 16:01 systemd-escape
-rwxr-xr-x 1 root root   14408 lis 15 16:01 systemd-notify
-rwxr-xr-x 1 root root   14408 sie 23 01:47 mountpoint
-rwxr-xr-x 1 root root   14416 lis 15 16:01 systemd-inhibit
-rwxr-xr-x 1 root root   18424 kwi 21  2017 efibootdump
-rwxr-xr-x 1 root root   18496 lis 15 16:01 systemd-machine-id-setup
-rwxr-xr-x 1 root root   18504 sty 31  2018 hostname
-rwxr-xr-x 1 root root   18760 gru 30  2017 run-parts
-rwxr-xr-x 1 root root   18872 sty 22  2018 openvt
-rwxr-xr-x 1 root root   19856 gru  1  2017 lesskey
-rwxr-xr-x 1 root root   23160 kwi 21  2017 getfacl
-rwxr-xr-x 1 root root   26632 sty 12  2018 whiptail
-rwsr-xr-x 1 root root   26696 sie 23 01:47 umount
-rwxr-xr-x 1 root root   26696 lis 15 16:01 systemd-tty-ask-password-agent
-rwxr-xr-x 1 root root   26704 sie  9 17:37 kill
-rwxr-xr-x 1 root root   26728 mar 21  2019 ntfscat
-rwxr-xr-x 1 root root   30744 mar 21  2019 ntfsusermap
-rwxr-xr-x 1 root root   30800 sie 23 01:47 wdctl
-rwsr-xr-x 1 root root   30800 sie 11  2016 fusermount
-rwxr-xr-x 1 root root   30824 mar 21  2019 ntfsmove
-rwxr-xr-x 1 root root   30904 sty 18  2018 true
-rwxr-xr-x 1 root root   30904 sty 18  2018 false
-rwxr-xr-x 1 root root   31928 mar 21  2019 ntfsls
-rwxr-xr-x 3 root root   34888 lip  4 14:35 bzip2
-rwxr-xr-x 3 root root   34888 lip  4 14:35 bzcat
-rwxr-xr-x 3 root root   34888 lip  4 14:35 bunzip2
-rwxr-xr-x 1 root root   34920 mar 21  2019 ntfscmp
-rwxr-xr-x 1 root root   34920 mar 21  2019 ntfscluster
-rwxr-xr-x 1 root root   34928 mar 21  2019 ntfsfallocate
-rwxr-xr-x 1 root root   35000 sty 18  2018 sync
-rwxr-xr-x 1 root root   35000 sty 18  2018 sleep
-rwxr-xr-x 1 root root   35000 sty 18  2018 pwd
-rwxr-xr-x 1 root root   35000 sty 18  2018 echo
-rwxr-xr-x 1 root root   35032 sty 18  2018 uname
-rwxr-xr-x 1 root root   35064 sty 18  2018 cat
-rwxr-xr-x 1 root root   35312 maj 14  2018 nc.openbsd
-rwxr-xr-x 1 root root   35512 kwi 21  2017 setfacl
-rwxr-xr-x 1 root root   35928 gru 11  2018 fuser
-rwxr-xr-x 1 root root   38904 kwi  4  2019 plymouth
-rwxr-xr-x 1 root root   38944 mar 21  2019 ntfstruncate
-rwxr-xr-x 1 root root   38952 sie 23 01:47 more
-rwxr-xr-x 1 root root   39103 kwi 23  2019 setupcon
-rwxr-xr-x 1 root root   40056 kwi 21  2017 efibootmgr
-rwxr-xr-x 1 root root   43080 lis 15 16:01 systemd-sysusers
-rwxr-xr-x 1 root root   43080 lis 15 16:01 networkctl
-rwsr-xr-x 1 root root   43088 sie 23 01:47 mount
-rwxr-xr-x 1 root root   43120 mar 21  2019 ntfsfix
-rwxr-xr-x 1 root root   43144 sty 22  2018 setfont
-rwxr-xr-x 1 root root   43192 sty 18  2018 rmdir
-rwxr-xr-x 1 root root   43192 sty 18  2018 readlink
-rwxr-xr-x 1 root root   43192 sty 18  2018 mktemp
-rwsr-xr-x 1 root root   44664 mar 22  2019 su
-rwxr-xr-x 1 root root   47752 mar 21  2019 ntfswipe
-rwxr-xr-x 1 root root   51280 lis 15 16:01 loginctl
-rwxr-xr-x 1 root root   51512 kwi 26  2016 ed
-rwxr-xr-x 1 root root   52664 mar 22  2019 login
-rwxr-xr-x 1 root root   55416 mar 21  2019 ntfsinfo
-rwxr-xr-x 1 root root   59608 sty 18  2018 chmod
-rwxr-xr-x 1 root root   63576 lis 15 16:01 journalctl
-rwxr-xr-x 1 root root   63672 sty 18  2018 chgrp
-rwxr-xr-x 1 root root   63704 sty 18  2018 rm
-rwsr-xr-x 1 root root   64424 cze 28 13:05 ping
-rwxr-xr-x 1 root root   64784 sie 23 01:47 findmnt
-rwxr-xr-x 1 root root   67768 sty 18  2018 mknod
-rwxr-xr-x 1 root root   67768 sty 18  2018 chown
-rwxr-xr-x 1 root root   67808 sty 18  2018 ln
-rwxr-xr-x 1 root root   71752 lis 15 16:01 systemd-tmpfiles
-rwxr-xr-x 1 root root   72000 sie 23 01:47 dmesg
-rwxr-xr-x 1 root root   75992 sty 18  2018 stty
-rwxr-xr-x 1 root root   76000 sty 18  2018 dd
-rwxr-xr-x 1 root root   80056 sty 18  2018 mkdir
-rwxr-xr-x 1 root root   80512 lis  5 19:09 mt-gnu
-rwxr-xr-x 1 root root   84048 sie 23 01:47 lsblk
-rwxr-xr-x 1 root root   84328 lis 15 16:01 systemd-hwdb
-rwxr-xr-x 1 root root   84776 sty 18  2018 df
-rwxr-xr-x 1 root root   88280 sty 18  2018 touch
-rwxr-xr-x 1 root root   88672 mar 21  2019 ntfssecaudit
-rwxr-xr-x 1 root root  100568 sty 18  2018 date
-rwxr-xr-x 1 root root  101560 kwi 28  2017 gzip
-rwxr-xr-x 1 root root  109000 sty 30  2018 sed
-rwxr-xr-x 1 root root  109232 mar 21  2019 lowntfs-3g
-rwxr-xr-x 1 root root  116840 mar 21  2019 ntfsrecover
-rwxr-xr-x 1 root root  121432 sty 25  2018 dash
-rwxr-xr-x 1 root root  133432 sie  9 17:37 ps
-rwxr-xr-x 1 root root  133792 sty 18  2018 vdir
-rwxr-xr-x 1 root root  133792 sty 18  2018 ls
-rwxr-xr-x 1 root root  133792 sty 18  2018 dir
-rwxr-xr-x 1 root root  137440 sty 18  2018 mv
-rwxr-xr-x 1 root root  139904 lut 26  2018 ss
-rwxr-xr-x 1 root root  141528 sty 18  2018 cp
-rwxr-xr-x 1 root root  146128 mar 21  2019 ntfs-3g
-rwxr-xr-x 1 root root  149688 lis 12  2018 kmod
-rwxr-xr-x 1 root root  154192 sty 10  2017 netstat
-rwxr-xr-x 1 root root  157224 lis  5 19:09 cpio
-rwxr-xr-x 1 root root  170520 sty 22  2018 dumpkeys
-rwxr-xr-x 1 root root  170760 gru  1  2017 less
-rwxr-xr-x 1 root root  182352 lis 15 16:01 systemctl
-rwxr-xr-x 1 root root  196632 wrz 10 23:25 hciconfig
-rwxr-xr-x 1 root root  211528 sty 22  2018 loadkeys
-rwxr-xr-x 1 root root  219456 wrz 18 17:00 grep
-rwxr-xr-x 1 root root  245872 mar  6  2018 nano
-rwxr-xr-x 1 root root  423312 sty 21  2019 tar
-rwxr-xr-x 1 root root  554104 lut 26  2018 ip
-rwxr-xr-x 1 root root  584072 lis 15 16:01 udevadm
-rwxr-xr-x 1 root root  748968 sie 29  2018 brltty
-rwxr-xr-x 1 root root 1113504 cze  7  2019 bash
-rwxr-xr-x 1 root root 2062296 mar  6  2019 busybox
